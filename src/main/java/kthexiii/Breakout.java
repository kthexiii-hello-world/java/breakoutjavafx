package kthexiii;

import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.scene.layout.Pane;
import javafx.util.Duration;

import static kthexiii.Constants.*;

import java.util.Random;

public class Breakout {
  Pane _root;

  Timeline _timeline;

  Block _blocks[];

  Paddle _paddle;
  Ball _ball;

  int _mouseX;

  Random rng;

  public Breakout() {
    _root = new Pane();
    rng = new Random();

    _paddle = new Paddle(WIDTH / 2 - PADDLE_WIDTH / 2, HEIGHT - PADDLE_BOTTOM_OFFSET);
    _ball = new Ball(WIDTH / 2, _paddle.getY() - BALL_RADIUS);

    _timeline = new Timeline(new KeyFrame(Duration.millis(TARGET_FRAMETIME), e -> {
      _ball.update();
      _paddle.setX(_mouseX);
      if (!_ball.getIsReleased()) {
        _ball.setX(_paddle.getCenterX());
        return;
      }

      if (_ball.getY() + BALL_RADIUS > _paddle.getY() && _ball.getY() - BALL_RADIUS < _paddle.getMaxY()
          && _ball.getX() + BALL_RADIUS > _paddle.getX() && _ball.getX() - BALL_RADIUS < _paddle.getMaxX()) {
        _ball.changeYDirection();

        // if (_ball.getX() + BALL_RADIUS > _paddle.getX() && _ball.getX() - BALL_RADIUS
        // < _paddle.getMaxX()
        // && !(_ball.getY() + BALL_RADIUS > _paddle.getY() && _ball.getY() -
        // BALL_RADIUS < _paddle.getMaxY()))
        // _ball.changeXDirection();
      }

      for (int i = 0; i < _blocks.length; i++) {
        var block = _blocks[i];
        boolean isCollision = false;

        if (_ball.getY() + BALL_RADIUS > block.getY() && _ball.getY() - BALL_RADIUS < block.getMaxY()
            && _ball.getX() + BALL_RADIUS > block.getX() && _ball.getX() - BALL_RADIUS < block.getMaxX()) {

          if (!block.isDead()) {
            _blocks[i].hit();
            // if (_ball.getX() + BALL_RADIUS > block.getX() && _ball.getX() - BALL_RADIUS <
            // block.getMaxX())
            // _ball.changeXDirection();
            _ball.changeYDirection();

            isCollision = true;
          }

        }

        if (isCollision)
          break;
      }

    }));
    _timeline.setCycleCount(Animation.INDEFINITE);
    _timeline.play();

    _root.setOnMouseMoved((e) -> {
      _mouseX = (int) e.getX();
    });
    _root.setOnMouseClicked(e -> {
      _ball.setIsReleased(true);
      _ball.incY(-1f);
      if (rng.nextDouble() > 0.5d)
        _ball.changeXDirection();

    });

    _blocks = new Block[BLOCK_ROWS * BLOCK_COLUMNS];
    for (int i = 0; i < BLOCK_ROWS; i++) {
      for (int j = 0; j < BLOCK_COLUMNS; j++) {
        _blocks[i * BLOCK_COLUMNS + j] = new Block(j * WIDTH / BLOCK_COLUMNS + BLOCK_OFFSET / 2,
            i * BLOCK_HEIGHT + BLOCK_OFFSET / 2);
        _root.getChildren().add(_blocks[i * BLOCK_COLUMNS + j].getNode());
      }
    }
    _root.getChildren().addAll(_paddle.getNode(), _ball.getNode());

  }

  public Pane getRoot() {
    return _root;
  }

}
