package kthexiii;

import javafx.scene.Node;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

import static kthexiii.Constants.*;

public class Block {
  Rectangle _rect;

  boolean _isDead = false;

  public Block(float x, float y) {
    _rect = new Rectangle(x, y, WIDTH / BLOCK_COLUMNS - BLOCK_OFFSET, BLOCK_HEIGHT - BLOCK_OFFSET);
    _rect.setCache(true);
    _rect.setFill(new Color(1, 1, 1, 1));
    _rect.setStroke(Color.TRANSPARENT);

    // _rects = new Rectangle[BLOCK_ROWS * BLOCK_COLUMNS];
    // for (int i = 0; i < BLOCK_ROWS; i++) {
    // for (int j = 0; j < BLOCK_COLUMNS; j++) {
    // _rects[i * BLOCK_COLUMNS + j] = new Rectangle(j * WIDTH / BLOCK_COLUMNS +
    // BLOCK_OFFSET / 2,
    // i * BLOCK_HEIGHT + BLOCK_OFFSET / 2, WIDTH / BLOCK_COLUMNS - BLOCK_OFFSET,
    // BLOCK_HEIGHT - BLOCK_OFFSET);
    // _rects[i * BLOCK_COLUMNS + j].setCache(true);
    // _rects[i * BLOCK_COLUMNS + j].setFill(new Color(1, 1, 1, 1));
    // _rects[i * BLOCK_COLUMNS + j].setStroke(Color.TRANSPARENT);
    // }
    // }
  }

  public Node getNode() {
    return _rect;
  }

  public float getX() {
    return (float) _rect.getX();
  }

  public float getY() {
    return (float) _rect.getY();
  }

  public void hit() {
    _isDead = true;
    _rect.setFill(new Color(22d / 255d, 22d / 255d, 22d / 255d, 1));
  }

  public boolean isDead() {
    return _isDead;
  }

  public float getMaxY() {
    return getY() + BLOCK_HEIGHT;
  }

  public float getMaxX() {
    return getX() + WIDTH / BLOCK_COLUMNS - BLOCK_OFFSET;
  }

}
