package kthexiii;

import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

import static kthexiii.Constants.*;

public class Paddle {
  Rectangle _rect;

  float _prevX;
  float _prevY;

  public Paddle(float x, float y) {
    _prevX = x;
    _prevY = y;

    _rect = new Rectangle(x, y, PADDLE_WIDTH, PADDLE_HEIGHT);
    _rect.setCache(true);
    _rect.setFill(Color.WHITE);
    _rect.setStroke(Color.TRANSPARENT);
  }

  public float getCenterX() {
    return (float) _rect.getX() + PADDLE_WIDTH / 2;
  }

  public float getCenterY() {
    return (float) _rect.getY() + PADDLE_HEIGHT / 2;
  }

  public void setX(float x) {
    _prevX = getX();

    _rect.setX(x - PADDLE_WIDTH / 2);
  }

  public float getX() {
    return (float) _rect.getX();
  }

  public float getY() {
    return (float) _rect.getY();
  }

  public float getMaxY() {
    return getY() + PADDLE_HEIGHT;
  }

  public float getMaxX() {
    return getX() + PADDLE_WIDTH;
  }

  public Rectangle getNode() {
    return _rect;
  }

}
