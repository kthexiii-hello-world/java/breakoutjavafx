package kthexiii;

import javafx.scene.Node;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;

import static kthexiii.Constants.*;

public class Ball {
  float _x;
  float _y;

  float _prevX;
  float _prevY;

  float x_dir = 1f;
  float y_dir = -1f;

  Circle _circle;

  boolean _isReleased = false;

  public Ball(float x, float y) {
    _x = x;
    _y = y;

    _circle = new Circle(x, y, BALL_RADIUS);
    _circle.setCache(true);
    _circle.setFill(Color.WHITE);
    _circle.setStroke(Color.TRANSPARENT);

  }

  public void update() {
    _prevX = _x;
    _prevY = _y;

    if (_isReleased) {
      _x += BALL_VX * x_dir;
      _y += BALL_VY * y_dir;
    }

    if (_x - BALL_RADIUS < 0) {
      _x = BALL_RADIUS;
      x_dir *= -1;
    }
    if (_x + BALL_RADIUS > WIDTH) {
      _x = WIDTH - BALL_RADIUS;
      x_dir *= -1;
    }
    if (_y - BALL_RADIUS < 0) {
      _y = BALL_RADIUS;
      y_dir *= -1;
    }
    if (_y + BALL_RADIUS > HEIGHT) {
      _y = HEIGHT - BALL_RADIUS;
      y_dir *= -1;
    }

    _circle.setCenterX(_x);
    _circle.setCenterY(_y);
  }

  public Circle getNode() {
    return _circle;
  }

  public float getX() {
    return _x;
  }

  public void setX(float x) {
    _x = x;
  }

  public float getY() {
    return _y;
  }

  public void setY(float y) {
    _y = y;
  }

  public void incY(float value) {
    _y += value;
  }

  public boolean getIsReleased() {
    return _isReleased;
  }

  public void setIsReleased(boolean isReleased) {
    _isReleased = isReleased;
  }

  public void changeXDirection() {
    x_dir *= -1;
  }

  public void changeYDirection() {
    y_dir *= -1;
  }

}
