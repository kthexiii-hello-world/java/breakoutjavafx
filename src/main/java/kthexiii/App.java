package kthexiii;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

import static kthexiii.Constants.*;

/**
 * Breakout Game in JavaFX
 *
 */
public class App extends Application {

    Pane _root;
    Scene _scene;
    Stage _stage;

    Breakout _breakout;

    @Override
    public void start(Stage primaryStage) throws Exception {
        _stage = primaryStage;
        _breakout = new Breakout();
        _root = _breakout.getRoot();

        _scene = new Scene(_root, WIDTH, HEIGHT);

        _scene.setFill(new Color(22d / 255d, 22d / 255d, 22d / 255d, 1));
        _stage.setScene(_scene);
        _stage.setTitle(TITLE);
        _stage.setResizable(false);
        _stage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }

}
