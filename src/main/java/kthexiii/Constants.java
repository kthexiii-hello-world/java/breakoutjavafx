package kthexiii;

/**
 * Application constants
 */
public interface Constants {
  static final String TITLE = "Breakout JavaFX";
  static final int HEIGHT = 480;
  static final int WIDTH = 640;

  final int BLOCK_COLUMNS = 8;
  final int BLOCK_ROWS = 5;
  final int BLOCK_OFFSET = 4;
  final int BLOCK_HEIGHT = 24;

  final int PADDLE_WIDTH = 100;
  final int PADDLE_HEIGHT = 16;
  final int PADDLE_BOTTOM_OFFSET = 35;

  final int BALL_RADIUS = 10;
  final float BALL_VX = 4f;
  final float BALL_VY = 4f;

  final double TARGET_FRAMETIME = 1000d / 60d; // ms
}
